// $Id: extlink_overlay.js,v 1 2012/04/28 01:25:56 $
(function ($) {
  Drupal.behaviors.bh_datepicker = {
    attach: function (context, settings) {
      $( ".pickadate" ).datepicker({
	      dateFormat: "dd-mm-yy",
	      minDate: 0,
	      maxDate: "+1Y",
	      changeMonth: true,
	      changeYear: true,
	      autoSize: true
      });
    }
  };
  
  Drupal.ajax.prototype.commands.scroll_to_wrapper = function(ajax, response, status) {
	if( response.ajax_return == true ) {
	    console.log('hotel_booking_response');
	    $(window).scrollTop($(response.scroll_to).offset().top);
	}
  };
  
  Drupal.behaviors.booking_iframe = {
    attach: function (context, settings) {
	  $(".book-hotel-continue").once().click(function(){
//		  var checkin = $('#get-dates-booking-form input[name=start_date]').val();
		  var hotel_id = $('#get-dates-booking-form input[name=hotel_id]').val();
		  var ajax_replace_wrapper = Drupal.settings.book_hotel.ajax_replace_wrapper;
		  console.log(ajax_replace_wrapper);
		  
		  var url = 'https://secure.booking.com/book.html?';
		  url += 'aid='+Drupal.settings.book_hotel.aid;
		  url += '&hostname=www.booking.com';
		  url += '&hotel_id='+hotel_id;
		  url += '&step=3';
		  url += '&stage=1';
		  url += '&checkin='+Drupal.settings.book_hotel.start_day;
		  url += '&interval='+Drupal.settings.book_hotel.interval;
		  url += '&test='+Drupal.settings.book_hotel.testmode;
		  var room_selected = false;
		  console.log(url);
		  $(".book_hotel_room_selection").each(function(index){
			  if ($(this).val() > 0) {
				  room_selected = true;
				  url += '&' + $(this).attr('name') + '=' + $(this).val();
			  }
		  });
		  if(room_selected) {
			  var replace_content = '';
			  replace_content += '<div id="book-hotel-iframe-popup-wrapper">';
			  replace_content += '<div class=book-hotel-iframe-popup-close>' + 'Close' + '</div>';
			  replace_content += '<h1 class=secure-booking-text>' + Drupal.settings.book_hotel.secure_booking_continue_text + '</h1>';
			  replace_content += '<iframe id="iframe-room-boorking-process" src="'+url+'" style="width:100%;min-height:100%;"></iframe>';
			  replace_content += '</div>'
			  if($('#book-hotel-iframe-popup-wrapper').length > 0) {
				  $('#book-hotel-iframe-popup-wrapper').replaceWith(replace_content);
			  }
			  else {
				  $('#' + ajax_replace_wrapper).append(replace_content);
			  }
			  $(window).scrollTop($('#book-hotel-iframe-popup-wrapper').offset().top);
		  }
		  else {
			  if($('.select-room-error-message').length > 0)
				  $('.select-room-error-message').html(Drupal.settings.book_hotel.none_selected_error);
			  else
				  $('#replace_hotelbook_div').append('<div class="select-room-error-message">' + Drupal.settings.book_hotel.none_selected_error + '</div>');
		  }
		  return false;
	  });
	  $('.book-hotel-iframe-popup-close').live('click', function() {
		  $('#book-hotel-iframe-popup-wrapper').remove();
		  var ajax_replace_wrapper = Drupal.settings.book_hotel.ajax_replace_wrapper;
		  console.log(ajax_replace_wrapper);
		  $(window).scrollTop($('#' + ajax_replace_wrapper).offset().top);
	  });
    }
  };
})(jQuery);
