<?php

/**
 * @file
 * Contains page callbacks for hotel_booking settings
 */

function book_hotel_admin_settings() {
  $form['book_hotel_test_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Test Mode ON'),
    '#default_value' => variable_get('book_hotel_test_mode', 1),
    '#description' => t('REMEMBER TO TURN THIS OFF ON PRODUCTION SITES. Keep this checked when developing the site and you do not wish to do actual hotel booking.'),
  );
  $form['book_hotel_affiliate_id'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Affiliate ID for Booking.com'),
    '#description' => t('Affiliate ID provided to you by Booking.com for using their services'),
    '#default_value' => variable_get('book_hotel_affiliate_id', ''),
  );
  $form['book_hotel_affiliate_user'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Affiliate Username for Booking.com'),
    '#description' => t('Affiliate Username provided to you by Booking.com for using their services'),
    '#default_value' => variable_get('book_hotel_affiliate_user', ''),
  );
  $form['book_hotel_affiliate_pass'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Affiliate Password for Booking.com'),
    '#description' => t('Affiliate Password provided to you by Booking.com for using their services'),
    '#default_value' => variable_get('book_hotel_affiliate_pass', ''),
  );
  $form['book_hotel_process_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Booking Process-block Title'),
    '#description' => t('Title to show for the block where booking process is handled'),
    '#default_value' => variable_get('book_hotel_process_title', t('Booking Process')),
  );
  $form['book_hotel_show_rooms_wrapper'] = array(
    '#type' => 'textfield',
    '#title' => t('Ajax wrapper'),
    '#description' => t('Wrapper for ajax load of hotel room details'),
    '#default_value' => variable_get('book_hotel_show_rooms_wrapper', 'replace_hotelbook_div'),
  );
  $form['book_hotel_room_currency'] = array(
    '#type' => 'textfield',
    '#title' => t('Room currency'),
    '#description' => t('Must be a valid three character uppercase ISO 4217 currencycode.'),
    '#default_value' => variable_get('book_hotel_room_currency', ''),
  );
  
  $form['book_hotel_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Hotel Booking Options'),
    '#description' => t('Arguments for booking.com function ') . l('http://distribution-xml.booking.com/affiliates/documentation/xml_getblockavailability.html', 'http://distribution-xml.booking.com/affiliates/documentation/xml_getblockavailability.html') . '.',
  );
  $form['book_hotel_options']['book_hotel_room_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show room image'),
    '#default_value' => variable_get('book_hotel_room_image', 0),
  );
  $form['book_hotel_options']['room_desc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Room Description options'),
  );
  $form['book_hotel_options']['room_desc']['book_hotel_room_desc'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show room description'),
    '#description' => t('THIS MUST BE CHECKED TO SHOW THE BELOW'),
    '#default_value' => variable_get('book_hotel_room_desc', 0),
  );
  $form['book_hotel_options']['room_desc']['book_hotel_important_information'] = array(
    '#type' => 'checkbox',
    '#title' => t('Important information'),
    '#description' => t('Returns additional information in hotel_text.'),
    '#default_value' => variable_get('book_hotel_important_information', 0),
  );
  $form['book_hotel_options']['room_desc']['book_hotel_split_block_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Split block text'),
    '#description' => t('Shows cancellation policy, facilities and taxes as a separate tags. Used in combination with parameter detail_level.'),
    '#default_value' => variable_get('book_hotel_split_block_text', 0),
  );
  $form['book_hotel_options']['room_details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Room Details options'),
  );
  $form['book_hotel_options']['room_details']['book_hotel_room_details'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show room details'),
    '#description' => t('THIS MUST BE CHECKED TO SHOW THE BELOW'),
    '#default_value' => variable_get('book_hotel_room_details', 0),
  );
  $form['book_hotel_options']['room_details']['book_hotel_show_max_children_free'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show max children free'),
    '#description' => t('Shows the maximum number of children allowed for free at the hotel.'),
    '#default_value' => variable_get('book_hotel_show_max_children_free', 0),
  );
  $form['book_hotel_options']['room_details']['book_hotel_show_max_children_free_age'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show max children free age'),
    '#description' => t('Shows the age limit used for deciding whether children stay for free in a room.'),
    '#default_value' => variable_get('book_hotel_show_max_children_free_age', 0),
  );
  $form['book_hotel_options']['room_details']['book_hotel_include_smoking_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include smoking info'),
    '#description' => t('If true, will include the smoking status.'),
    '#default_value' => variable_get('book_hotel_include_smoking_status', 0),
  );
  $form['book_hotel_options']['room_details']['book_hotel_include_internet'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include internet info'),
    '#description' => t('Includes internet/wifi availability'),
    '#default_value' => variable_get('book_hotel_include_internet', 0),
  );
  $form['book_hotel_options']['book_hotel_show_addons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show addons'),
    '#description' => t('Show addon information for blocks. This includes information about adding an extra bed, breakfast, etc. and its price. Note: not all hotels have addons available.'),
    '#default_value' => variable_get('book_hotel_show_addons', 0),
  );
  $form['book_hotel_options']['book_hotel_include_cc_required'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include CC required'),
    '#description' => t('Shows CC Required field for hotels.'),
    '#default_value' => variable_get('book_hotel_include_cc_required', 0),
  );
  $form['book_hotel_options']['book_hotel_additional_room_info'] = array(
    '#type' => 'checkbox',
    '#title' => t('Additional room info'),
    '#description' => t('Shows the room size (m2, feet) and bedding info. Must be combined with the parameter detail_level.'),
    '#default_value' => variable_get('book_hotel_additional_room_info', 0),
  );
  
  return system_settings_form($form);
}
